# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=py3-pikepdf
_pyname=pikepdf
pkgver=6.0.2
pkgrel=0
pkgdesc="Python library for reading and writing PDF"
url="https://github.com/pikepdf/pikepdf"
arch="all"
license="MPL-2.0"
depends="
	py3-deprecation
	py3-lxml
	py3-packaging
	py3-pillow
	python3
	"
makedepends="
	py3-gpep517
	py3-installer
	py3-pybind11-dev
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	python3-dev
	qpdf-dev
	"
checkdepends="
	py3-hypothesis
	py3-psutil
	py3-pytest
	py3-pytest-xdist
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/pikepdf/pikepdf/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver

# secfixes:
#   2.9.1-r2:
#     - CVE-2021-29421

build() {
	mkdir dist
	python3 -m gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	python -m installer -d test_install \
		dist/pikepdf-*.whl
	PYTHONPATH="$(echo $PWD/test_install/usr/lib/python3*/site-packages)" pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/pikepdf-*.whl
}

sha512sums="
d5f778bd0461ebe7f505f39ee8c171fd8845a181e5e5a262b4492ab68db95fad18f511143d313813712288b2101170bbbeb92bcfbe2d349a163e1acafa23c1dd  py3-pikepdf-6.0.2.tar.gz
"
