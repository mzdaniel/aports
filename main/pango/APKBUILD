# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=pango
pkgver=1.50.10
pkgrel=0
pkgdesc="library for layout and rendering of text"
url="https://www.pango.org/"
arch="all"
license="LGPL-2.1-or-later"
depends_dev="pango-tools=$pkgver-r$pkgrel"
makedepends="
	cairo-dev
	expat-dev
	fontconfig-dev
	fribidi-dev
	glib-dev
	gobject-introspection-dev
	gtk-doc
	harfbuzz-dev
	help2man
	libxft-dev
	meson
	"
checkdepends="ttf-dejavu ttf-cantarell ttf-droid ttf-tlwg"
install="$pkgname.pre-deinstall"
triggers="$pkgname.trigger=/usr/lib/pango/*/modules"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-doc $pkgname-tools"
source="https://download.gnome.org/sources/pango/${pkgver%.*}/pango-$pkgver.tar.xz
	disable-broken-test.patch
	"

# secfixes:
#   1.44.1-r0:
#     - CVE-2019-1010238

build() {
	abuild-meson \
		-Db_lto=true \
		-Dintrospection=enabled \
		-Dgtk_doc=false \
		build
	meson compile ${JOBS:+-j ${JOBS}} -C build
}

check() {
	meson test --no-rebuild --print-errorlogs -C build
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C build
}

tools() {
	pkgdesc="$pkgdesc (tools)"

	amove usr/bin
}

sha512sums="
38e5c96ef89681ca72367a96bc2d2083df2cbe861b8725fb02d0f6383300e6cda4133d58e39cdc505c712c008ae708d272e3e75380f14e9850b45f56d5d89338  pango-1.50.10.tar.xz
3623ef34e08427c70b4f2f89eefe13258646a935c450d9ad703c30018343aef245749e6a3708f299fd696d9f1ec0eeab116131e51918fc1765f61545202aaab0  disable-broken-test.patch
"
