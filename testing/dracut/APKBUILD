# Maintainer: Conrad Hoffmann <ch@bitfehler.net>
pkgname=dracut
pkgver=057
pkgrel=0
pkgdesc="An event driven initramfs infrastructure"
url="https://github.com/dracutdevs/dracut/wiki"
arch="all !riscv64" # textrel
license="GPL-2.0-or-later"
depends="bash eudev coreutils findmnt blkid"
makedepends="asciidoc musl-fts-dev kmod-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-bash-completion"
options="!check" # There is a test suite, but it requires root/sudo
triggers="$pkgname.trigger=/usr/share/kernel/*"
source="$pkgname-$pkgver.tar.gz::https://github.com/dracutdevs/dracut/archive/refs/tags/$pkgver.tar.gz"
provides="initramfs-generator"
provider_priority=100 # low, somewhat experimental

build() {
	./configure --sysconfdir="/etc"
	CFLAGS="$CFLAGS -D__GLIBC_PREREQ=" CWD="$(pwd)" make
}

package() {
	DESTDIR="$pkgdir" make install
}

sha512sums="
8acdc8db2233a9abbaeea218cc5b1be68c4985088995f42624750783f8d40ecbb7fa97ab4f6468f67c079c8418590ace317c143a92d9305640b48c7c0edd4089  dracut-057.tar.gz
"
